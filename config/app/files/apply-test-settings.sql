--  Remove Base URL and any CDN settings
DELETE
FROM `core_config_data`
WHERE `path` LIKE 'web/%secure/base_%url';

-- Remove Cookie domain
DELETE
FROM `core_config_data`
WHERE path IN ('web/cookies/cookie_domain');
DELETE
FROM `core_config_data`
WHERE `path` = 'web/cookie/cookie_domain'
  AND `scope` = 'websites';

-- Set test config settings
REPLACE INTO `core_config_data` (`path`, `value`, `scope`, `scope_id`)
VALUES
    -- Allow for longer admin sessions
       ('admin/security/session_cookie_lifetime', 86400, 'default', 0),
    -- Disable robots.txt
       ('design/head/default_robots', 0, 'default', 0),
    -- Enable logs
       ('dev/log/active', 1, 'default', 0),
    -- Disable Google Analytics and Tag Manager
       ('google/analytics/active', 0, 'default', 0),
       ('google/tagmanager/active', 0, 'default', 0),
    -- Allow template symlinks
       ('dev/template/allow_symlink', 1, 'default', 0),
    -- Activate Reflektion Dev Beacon
       ('hankypanky_beacon/general/enabled', 1, 'default', 0),
    -- Disable the mega menu cache
       ('megamenu/general/cache', 0, 'default', 0),
    -- Clear the session cookie management
       ('web/cookie/cookie_lifetime', '', 'default', 1),
       ('web/cookie/cookie_path', '', 'default', 1),
       ('web/cookie/cookie_httponly', '', 'default', 1),
       ('web/cookie/cookie_domain', '', 'default', 1),
    -- Update from emails to robofirm.net emails so that they can send via Amazon SES
       ('trans_email/ident_general/email', 'owner@robofirm.net', 'default', 0),
       ('trans_email/ident_sales/email', 'sales@robofirm.net', 'default', 0),
       ('trans_email/ident_custom1/email', 'custom1@robofirm.net', 'default', 0),
       ('trans_email/ident_custom2/email', 'custom2@robofirm.net', 'default', 0),
       ('contact/email/recipient_email', 'dev@localhost.com', 'default', 0),
    -- Clear out systems integration ftp user, password, and disable ftp
       ('systemsintegration_general/settings/ftp_host', '', 'default', 0),
       ('systemsintegration_general/settings/ftp_login', '', 'default', 0),
       ('systemsintegration_general/settings/ftp_password', '', 'default', 0),
       ('systemsintegration_general/settings/ftp_disabled', 1, 'default', 0),
    -- Disable cronjobs
       ('system/cron/enable', 0, 'default', 0),  
    -- Update Authorize.net CIM Sandbox
       ('payment/authnetcim/test', 1, 'default', 0),
       ('payment/authnetcim/login', '0:2:40808b2b3d84e8d0:O71hvLZwUKdTMcOcYeFywg==', 'default', 0),
       ('payment/authnetcim/trans_key', '0:2:640428ee4f30ec8d:sT/VIjuTxUMKakQ5R460bQ==', 'default', 0),
       ('payment/authnetcim/verify_ssl', 0, 'default', 0),
    -- Update Amasty Feed Settings
       ('systemsintegration_general/catfeed/enabled', 0, 'default', 0),
       ('systemsintegration_general/catfeed/host', '', 'default', 0),
       ('systemsintegration_general/catfeed/user', '', 'default', 0),
       ('systemsintegration_general/catfeed/password', '', 'default', 0),
       ('systemsintegration_general/catfeed/path', '', 'default', 0)
       ;

-- Remove Reflecktions and Border Free
-- Set test config settings
REPLACE INTO `am_feed_profile` (`feed_id`, `ftp_host`, `ftp_user`, `ftp_pass`, `ftp_folder`)
VALUES
   (11, '', '', '',''),
   (15, '', '', '',''),
   (16, '', '', '',''),
   (22, '', '', '','')
   ;

-- Create dev Magento admin user
REPLACE INTO `admin_user` (`firstname`, `lastname`, `email`, `username`, `password`, `created`, `is_active`)
VALUES ('Robofirm', 'Dev', 'dev@localhost.com', 'dev', (SELECT MD5('password1')), NOW(), 1);
REPLACE INTO `admin_role` (`parent_id`, `tree_level`, `sort_order`, `role_type`, `user_id`, `role_name`)
VALUES (1, 2, 0, 'U', (SELECT last_insert_id()), 'Robofirm Dev');

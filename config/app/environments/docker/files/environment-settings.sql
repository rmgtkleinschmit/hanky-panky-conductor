-- Update Magento store URLs and cookie domains
REPLACE INTO `core_config_data` (`path`, `value`, `scope`, `scope_id`) VALUES

    -- Base URLs
    ('web/unsecure/base_url', 'https://local-hankypanky.robofirm.net/', 'default', 0),
    ('web/secure/base_url', 'https://local-hankypanky.robofirm.net/', 'default', 0),
    ('web/cookie/cookie_domain', 'local-hankypanky.robofirm.net', 'default', 0),

    -- Allow template symlinks
    ('dev/template/allow_symlink', 1, 'default', 0)
;

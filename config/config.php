<?php
 
use ConductorCore\Crypt\Crypt;
use ConductorCore\YamlFileProvider;
use Zend\ConfigAggregator\ArrayProvider;
use Zend\ConfigAggregator\ConfigAggregator;
use Zend\ConfigAggregator\PhpFileProvider;
 
if (file_exists(__DIR__ . '/env.php')) {
    $environmentConfig = include __DIR__ . '/env.php';
}
 
$environment = $environmentConfig['environment'] ?? 'development';
$cryptKey = $environmentConfig['crypt_key'] ?? null;
 
// To enable or disable caching, set the `ConfigAggregator::ENABLE_CACHE` boolean in
// `config/autoload/local.php`.
$cacheConfig = [
    'config_cache_path' => 'data/config-cache.php',
];
 
$aggregator = new ConfigAggregator(
    [
        \Zend\Expressive\ConfigProvider::class,
        \Zend\HttpHandlerRunner\ConfigProvider::class,
        \Zend\Expressive\Router\ZendRouter\ConfigProvider::class,
        \Zend\Expressive\Helper\ConfigProvider::class,
        \Zend\Expressive\Router\ConfigProvider::class,
        \ConductorMagento1PlatformSupport\ConfigProvider::class,
        \ConductorMySqlSupport\ConfigProvider::class,
        \ConductorAppOrchestration\ConfigProvider::class,
        \ConductorGitVcsSupport\ConfigProvider::class,
        \ConductorAwsS3FilesystemSupport\ConfigProvider::class,
        \ConductorCore\ConfigProvider::class,
        \Zend\Router\ConfigProvider::class,
        \Zend\Validator\ConfigProvider::class,
        // Include cache configuration
        new ArrayProvider($cacheConfig),
        // Default App module config
        App\ConfigProvider::class,
        // Load application config in a pre-defined order in such a way that local settings
        // overwrite global settings. (Loaded as first to last):
        //   - `global.php`
        //   - `*.global.php`
        //   - `environments/*/*.php`
        //   - `local.php`
        //   - `*.local.php`
        // @todo Add environment config here
        new PhpFileProvider('config/autoload/{,*.}global.php'),
        Crypt::decryptExpressiveConfig(new YamlFileProvider('config/app/{,*.}yaml'), $cryptKey),
        Crypt::decryptExpressiveConfig(new YamlFileProvider('config/app/environments/' . $environment . '/{,*.}yaml'), $cryptKey),
        new PhpFileProvider('config/autoload/{,*.}local.php'),
        // Load development config if it exists
        new PhpFileProvider('config/development.config.php'),
        new ArrayProvider(['environment' => $environment, 'crypt_key' => $cryptKey]),
    ], $cacheConfig['config_cache_path']
);
 
return $aggregator->getMergedConfig();
